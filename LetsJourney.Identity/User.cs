﻿using Microsoft.AspNetCore.Identity;

namespace LetsJourney.Identity;

public class User : IdentityUser
{
    public User(string userName)
    {
        base.UserName = userName;
    }
}

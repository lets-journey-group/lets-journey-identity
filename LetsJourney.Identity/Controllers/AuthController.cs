﻿using Duende.IdentityServer.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LetsJourney.Identity.Controllers;
[Route("api/[controller]")]
[ApiController]
public class AuthController : ControllerBase
{
    private readonly SignInManager<User> signInManager;
    private readonly UserManager<User> userManager;
    private readonly IIdentityServerInteractionService interactionService;

    public AuthController(
        SignInManager<User> signInManager,
        UserManager<User> userManager,
        IIdentityServerInteractionService interactionService)
    {
        this.signInManager = signInManager;
        this.userManager = userManager;
        this.interactionService = interactionService;
    }

    [HttpPost("register")]
    public async Task<IActionResult> Register([FromQuery] string login, [FromQuery] string password)
    {
        User user = new(login);
        var res = await userManager.CreateAsync(user, password);
        if (res.Succeeded)
        {
            return Ok();
        }

        return BadRequest(res.Errors);
    }

    //[HttpPost("login")]
    //public async Task<IActionResult> Login([FromQuery] string login, [FromQuery] string password)
    //{
    //    var user = await userManager.FindByNameAsync(login);
    //    var signInResult = await signInManager.PasswordSignInAsync(user, password, false, false);
    //    Console.WriteLine(JsonSerializer.Serialize(signInResult));

    //    //List<Claim> claims = new()
    //    //{
    //    //    new Claim("scope", "LetsJourneyWebApi"),
    //    //    new Claim("LetsJourneyWebApi")
    //    //};

    //    //await signInManager.SignInWithClaimsAsync(createdUser, false, claims);
    //    return Ok();
    //}
}

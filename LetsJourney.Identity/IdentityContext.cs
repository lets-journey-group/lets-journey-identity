﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LetsJourney.Identity;

public class IdentityContext : IdentityDbContext<User>
{
    private const string _connectionString
    //    = "Server=(localdb)\\mssqllocaldb;Database=usersdirectorydb;Trusted_Connection=True;";
        = "Host=identitydb;Port=5432;Database=identitydb;Username=postgres;Password=password";

    public IdentityContext(DbContextOptions<IdentityContext> options)
        : base(options)
    {
        Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(_connectionString);
    }
}

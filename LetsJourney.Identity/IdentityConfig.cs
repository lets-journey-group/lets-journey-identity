﻿using Duende.IdentityServer;
using Duende.IdentityServer.Models;

namespace LetsJourney.Identity;

public static class IdentityConfig
{
    public static List<ApiScope> ApiScopes =>
        new()
        {
            new ApiScope("LetsJourneyWebApi", "Web Api"),
            new ApiScope(IdentityServerConstants.StandardScopes.OpenId, "Open Id")
        };

    public static List<Client> Clients =>
        new()
        {
            new()
            {
                ClientId = "Mobile",
                AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                ClientSecrets =
                {
                    new("mobile secret".Sha256())
                },
                AllowedScopes =
                {
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    "LetsJourneyWebApi"
                },
                AlwaysIncludeUserClaimsInIdToken = true,
                //AllowAccessTokensViaBrowser = true,
                //AllowedCorsOrigins =
                //{
                //    ""
                //},
            }
        };
}


FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["LetsJourney.Identity/LetsJourney.Identity.csproj", "LetsJourney.Identity/"]
RUN dotnet restore "LetsJourney.Identity/LetsJourney.Identity.csproj"
COPY . .
WORKDIR "/src/LetsJourney.Identity"
RUN dotnet build "LetsJourney.Identity.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "LetsJourney.Identity.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "LetsJourney.Identity.dll"]
